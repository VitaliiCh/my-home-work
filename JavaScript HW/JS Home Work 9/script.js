let tabText = document.querySelectorAll('.tabs-title');
tabText = Array.from(tabText); // [div, div, ...]

let tabContent = document.querySelectorAll('.tabs-content');

for (i = 0; i < tabText.length; i++) {
    tabText[i].addEventListener('click', function (e) {
        for (i = 0; i < tabContent.length; i++) {
            tabContent[i].style.display = 'none';
        }
        let el = e.target;
        let index = tabText.indexOf(el);
        tabContent[index].style.display = 'block';
    });
}




